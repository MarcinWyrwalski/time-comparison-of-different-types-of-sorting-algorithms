package pl.Chomik.speedTestOfDiffrentSortingMethod;

import java.util.*;

public class TestArena {

    private static Random rand = new Random(500);
    private static long numbersOfOperations;


    private static int selectionSort(int[] array) {
        int minvalue;
        int minValueIndex;
        int counter = 0;

        for (int i = 0; i < array.length; i++) {
            minvalue = array[i];
            minValueIndex =i;
            for (int y = i; y < array.length; y++) {
                if (minvalue > array[y]) {
                    minvalue = array[y];
                    minValueIndex = y;
                    counter++;
                }

            }

            int temp = array[i];
            array[i] = minvalue;
            array[minValueIndex] = temp;
            counter++;
        }

        return counter;
    }

    public static void main(String[] args) {


        List<OperationData> dk  = new LinkedList<OperationData>();

        boolean hasContinue = true;
        do {



        Scanner sc = new Scanner(System.in);
        System.out.println("Number of operations in base: "+dk.size());
        System.out.println("Podaj typ sortowania: 1 dla BubleSort, 2 dla  SelectionSort, 9 dla operation display; 0 Exit ");
        int choice = sc.nextInt();
        if (choice == 9){
            System.out.println(dk);
            continue;
        } else if (choice == 0){
            System.exit(9);

        }
        System.out.println("Podaj ilosc liczb do badnia: ");
        int userDeclaredArraySize = sc.nextInt();
        System.out.println("Podaj liczbę powtórzeń:  ");
        int userDeclaredNumberOfRepetition = sc.nextInt();


        switch (choice) {
            case 1:
                int counter = 0;
                while (counter < userDeclaredNumberOfRepetition) {
                    long startTimeInMiliSec = System.currentTimeMillis();
                    numbersOfOperations = 0;
                    numbersOfOperations = bubleSort(createArray(userDeclaredArraySize));
                    long sortingRunningTime = System.currentTimeMillis() - startTimeInMiliSec;
                    OperationData od = new OperationData(SORT.BUBBLE_SORT, userDeclaredArraySize, sortingRunningTime, numbersOfOperations);
                    dk.add(od);
                    counter++;
                }
                break;

            case 2:
                counter = 0;
                while (counter < userDeclaredNumberOfRepetition) {
                    long startTimeInMiliSec = System.currentTimeMillis();
                    numbersOfOperations = 0;
                    numbersOfOperations = selectionSort(createArray(userDeclaredArraySize));
                    long endTimeInMiliSec = System.currentTimeMillis();
                    System.out.println("Array contains " + userDeclaredArraySize + " numbers, was sorted in: " + (endTimeInMiliSec - startTimeInMiliSec) + " milisecond");
//                    int numberOfOperations =0;
                    System.out.println("iteration nr: " + counter + " NumberOfIteration: " + numbersOfOperations);
                    counter++;
                }
                break;

//            case 8:
//                counter = 0;
//                while (counter < userDeclaredNumberOfRepetition) {
//                    long startTimeInMiliSec = System.currentTimeMillis();
//                    numbersOfOperations = 0;
//                    numbersOfOperations = selectionSort(createArray(userDeclaredArraySize));
//                    long endTimeInMiliSec = System.currentTimeMillis();
//                    System.out.println("Array contains " + userDeclaredArraySize + " numbers, was sorted in: " + (endTimeInMiliSec - startTimeInMiliSec) + " milisecond");
////                    int numberOfOperations =0;
//                    System.out.println("iteration nr: " + counter + " NumberOfIteration: " + numbersOfOperations);
//                    counter++;
//                }
//                break

        }


        }while (hasContinue);
    }



    private static int[] createArray(int expectedArraySize) {
        int[] numbers = new int[expectedArraySize];
        for (int i = 0; i < expectedArraySize; i++) {
            numbers[i] = rand.nextInt(100);
        }
        return numbers;
    }


    private static long bubleSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int y = 0; y < array.length - 1; y++) {
                if (array[y] > array[y + 1]) {
                    int temp = array[y];
                    array[y] = array[y + 1];
                    array[y + 1] = temp;
                    numbersOfOperations++;
                }

            }
        }
        return numbersOfOperations;

    }
}
